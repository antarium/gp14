# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2019-08-12 13:28
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20190809_1134'),
    ]

    operations = [
        migrations.CreateModel(
            name='DocumentsStore',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('label', models.CharField(default='link to document', help_text='Текст ссылки на документ', max_length=255)),
                ('caption', models.CharField(blank=True, help_text='Заголовок (необязателен) для сслыки', max_length=255, null=True)),
            ],
            options={
                'verbose_name': 'Документ',
                'verbose_name_plural': 'Документы для страниц сайтов',
            },
        ),
        migrations.CreateModel(
            name='OfitsialnyeRekvizity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('label', models.CharField(max_length=255)),
                ('text', models.TextField(default='')),
            ],
        ),
        migrations.RenameModel(
            old_name='FilsStore',
            new_name='FilesStore',
        ),
        migrations.AlterModelOptions(
            name='articlesnews',
            options={'ordering': ['-created'], 'verbose_name': 'Новость', 'verbose_name_plural': 'Новости'},
        ),
        migrations.AlterField(
            model_name='articlesnews',
            name='image',
            field=models.ImageField(blank=True, help_text='Лучше использовать картинки с одинаковыми высотой и шириной', null=True, upload_to='files/%Y/%m/%d'),
        ),
        migrations.AlterField(
            model_name='articlesnews',
            name='text',
            field=ckeditor.fields.RichTextField(),
        ),
        migrations.AddField(
            model_name='documentsstore',
            name='document',
            field=models.OneToOneField(help_text='Указатель на загруженный файл', on_delete=django.db.models.deletion.CASCADE, to='core.FilesStore'),
        ),
        migrations.AddField(
            model_name='documentsstore',
            name='page',
            field=models.OneToOneField(help_text='Указатель на страницу, которой принадлежит документ', on_delete=django.db.models.deletion.CASCADE, to='core.TopicButtons'),
        ),
    ]
