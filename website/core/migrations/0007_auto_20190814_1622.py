# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2019-08-14 16:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20190814_1619'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentsstore',
            name='caption',
            field=models.CharField(blank=True, help_text='Заголовок (необязателен) для сслыки', max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='filesstore',
            name='about',
            field=models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Краткое описание (необязательно)'),
        ),
        migrations.AlterField(
            model_name='filesstore',
            name='caption',
            field=models.CharField(blank=True, default='', max_length=255, null=True, verbose_name='Название файла (необязательно)'),
        ),
    ]
