from core.models import TopicButtons


class TopicsMiddleware(object):
    """

    Отвечает за подгрузку из БД данных о пунктах меню
    работа с контекстом на фронте осуществляется в base.html

    """

    def process_template_response(self, request, response):
        # получается куча запросов в БД, поэтому результат надо кэшировать
        topics = TopicButtons.objects.filter(second_level_link=False)
        # затем для каждого топика получаем список подтем
        topics = [{"topic": item, 
                "subtopic": TopicButtons.objects.filter(parent=item)}
            for item in topics]
        response.context_data['topics'] = topics
        return response


class LowVisionStartPage(object):
    """

    Отвечает за установку куки для режима слабовидящих в False для случаев,
    когда куки сайтов очищены

    """
    """
    def process_request(self, request):
        print(request.session.get("low_vision_mode", None))
        if request.session.get("low_vision_mode", None) is None:
            request.session["low_vision_mode"] = True 

    """
    def process_template_response(self, request, response):
        # получается куча запросов в БД, поэтому результат надо кэшировать
        if request.session.get("low_vision_mode", None) is None:
            request.session["low_vision_mode"] = True 
        return response
    