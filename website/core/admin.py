from django.contrib import admin
#from markitup.widgets import AdminMarkItUpWidget
from core.models import *

# Register your models here.


class FilesStoreAdmin(admin.ModelAdmin):
    model = FilesStore
    list_display = ('about', 'caption', 'file', )
    search_fields = ('caption', )


class SliderAdmin(admin.ModelAdmin):
    model = Slider
    list_display = ('title', 'text', 'url','image', )


class TopicButtonsAdmin(admin.ModelAdmin):
    model = TopicButtons
    list_display = ('id', 'caption', 'url', 'parent','second_level_link','order', )
    list_filter = ('second_level_link', )


class ArticlesNewsAdmin(admin.ModelAdmin):
    model = "News"
    list_display = ('created', 'title', 'preview', 'image', )


class OfitsialnyeRekvizityAdmin(admin.ModelAdmin):
    model = "OfitsialnyeRekvizity"
    list_display = ('label', 'text',)


class DocumentsStoreAdmin(admin.ModelAdmin):
    model = "DocumentsStore"
    list_display = ('label', 'caption', 'page', 'document',)
    search_fields = ('label', 'page',)
    list_filter = ('page',)


admin.site.register(FilesStore, FilesStoreAdmin)
admin.site.register(Slider, SliderAdmin)
admin.site.register(TopicButtons, TopicButtonsAdmin)
admin.site.register(ArticlesNews, ArticlesNewsAdmin)
admin.site.register(OfitsialnyeRekvizity, OfitsialnyeRekvizityAdmin)
admin.site.register(DocumentsStore, DocumentsStoreAdmin)