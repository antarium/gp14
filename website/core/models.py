from django.db import models
from ckeditor.fields import RichTextField
#from markitup.fields import MarkupField

# Create your models here.


class FilesStore(models.Model):

    class Meta:
        verbose_name = "Файлопомойка"
        verbose_name_plural = "Загруженные файлы"

    about = models.CharField(max_length=255, default="", null=True, blank=True,
        verbose_name="Краткое описание (необязательно)")
    caption = models.CharField(max_length=255, default="",
        null=True, blank=True,
        verbose_name="Название файла (необязательно)")
    file = models.FileField(verbose_name='Файл', upload_to='files/%Y/%m/%d')

    def __str__(self):
        return f"file {self.caption} - {self.about}"


class OfitsialnyeRekvizity(models.Model):

    class Meta:
        verbose_name = "Реквизит"
        verbose_name_plural = "Официальные реквизиты"
        ordering = ("id",)

    #label = models.CharField(max_length=255)
    #text = models.TextField(default="")
    label = RichTextField()
    text = RichTextField()


class Slider(models.Model):

    class Meta:
        verbose_name = "Слайд"
        verbose_name_plural = "Слайдер"

    title = models.CharField(max_length=255, verbose_name="Заголовок")
    text = models.CharField(max_length=255, verbose_name="Текст")
    url = models.CharField(max_length=255, verbose_name="ссылка", blank=True,
        null=True,
        help_text="Если пустой, то кнопка перехода на слайде не отображается")
    image = models.ImageField(upload_to='slider/%Y/%m/%d',
        verbose_name='Картинка',
        help_text="Рекомендуется загружать картинки с прозрачным фоном в .png")


class TopicButtons(models.Model):

    class Meta:
        verbose_name = "Пункт меню"
        verbose_name_plural = "Основное меню"
        ordering = ['order', 'id']

    caption = models.CharField(max_length=255, verbose_name="Название")
    url = models.CharField(max_length=255, verbose_name="ссылка")
    parent = models.ForeignKey('self', blank=True, null=True,
        verbose_name='Вложено в ', on_delete=models.CASCADE)
    second_level_link = models.BooleanField(default = False,
        verbose_name='Вложенный пункт меню',
        help_text="True - для подпунктов меню")
    order = models.IntegerField(verbose_name='порядок', default=100)

    def __str__(self):
        return self.caption


class DocumentsStore(models.Model):

    class Meta:
        verbose_name = "Документ"
        verbose_name_plural = "Документы для страниц сайтов"
        ordering = ("id",)

    document = models.OneToOneField(FilesStore, on_delete=models.CASCADE,
        help_text="Указатель на загруженный файл")
    label = models.CharField(max_length=255, default="link to document",
        help_text="Текст ссылки на документ")
    page = models.ForeignKey(TopicButtons, on_delete=models.CASCADE,
        help_text="Указатель на страницу, которой принадлежит документ")
    caption = models.CharField(max_length=255, null=True, blank=True,
        help_text="Заголовок (необязателен) для сслыки")


class ArticlesNews(models.Model):

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"
        ordering = ["-created"]

    title = models.CharField(max_length=255, verbose_name="Заголовок")
    preview = models.TextField(default="", verbose_name="Короткий текст")
    image = models.ImageField(upload_to='files/%Y/%m/%d', null=True, blank=True,
        help_text="Лучше использовать картинки с одинаковыми высотой и шириной")
    created = models.DateTimeField(auto_now_add=True)
    #text = models.TextField(default="")
    text = RichTextField()

