import os
from django.conf import settings
from django.utils.decorators import classonlymethod
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render
from django.views.generic import View, TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from core.models import *

# Create your views here.


class IndexPage(ListView):
    template_name = "index.html"
    queryset = ArticlesNews.objects.all()[:10]

    def get(self, request, *args, **kwargs):
        return super(IndexPage, self).get(request)

    def get_context_data(self, **kwargs):
        context = super(IndexPage, self).get_context_data(**kwargs)
        context["slider"] = Slider.objects.all()
        return context


class NewsPage(ListView):
    template_name = "news.html"
    queryset = ArticlesNews.objects.all()


class CurrentNew(DetailView):
    template_name = "current_new.html"
    model = ArticlesNews


class ChangemodeView(View):

    def get(self, request, *args, **kwargs):
        try:
            old_url = request.META["HTTP_REFERER"]
        except:
            old_url = '/'
        # сменяем значение куки
        low_vision_mode = not request.session.get("low_vision_mode", False)
        request.session["low_vision_mode"] = low_vision_mode
        request.session.set_expiry(0)
        return HttpResponseRedirect(old_url)


class RekvizityPage(ListView):
    template_name = "about/rekvizity.html"
    model = OfitsialnyeRekvizity

    def get_context_data(self, **kwargs):
        context = super(RekvizityPage, self).get_context_data()
        context["documents"] = DocumentsStore.objects.filter(page=2)
        return context


class CustomTemplatePage(TemplateView):
    """Универсальное представление для собственных шаблонов"""
    page_id = None

    def get_context_data(self, **kwargs):
        context = super(CustomTemplatePage, self).get_context_data()
        context["documents"] = DocumentsStore.objects.filter(page=self.page_id)
        return context

    @classonlymethod
    def run_view(cls, template_name, id_page, **initkwargs):
        print("runView")
        cls.page_id = id_page
        cls.template_name = template_name
        return cls.as_view()


class CommonTemplatePage(TemplateView):
    """Универсальное представление для страниц, сделанных через WYSIWYG"""
    template_name = "common_templates/page_in_development.html"

    def get(self, request, *args, **kwargs):
        url = request.META['PATH_INFO']
        obj = TopicButtons.objects.filter(url=url).first()
        if obj is None:
            raise Http404("Такая страница не найдена")
        kwargs.update(page_title=obj.caption)
        return super(CommonTemplatePage, self).get(request, *args, **kwargs)
