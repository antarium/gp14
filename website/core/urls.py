from django.views.generic import View, TemplateView
from django.conf.urls import url
from core.views import *


urlpatterns = [
	url(r'^news/(?P<pk>\d+)/$', CurrentNew.as_view(), name='current_new'),
	url(r'^news/$', NewsPage.as_view(), name='news'),
	url(r'^change_mode/$', ChangemodeView.as_view(), name='changemode'),
	url(r'^about/ofitsialnye-rekvizity/$', RekvizityPage.as_view(),
		name='of_rekvisity'),
	# собственные шаблоны для страниц -----------
	url(r"^about/kvalifikatsionnye-kategorii/",
		CustomTemplatePage.run_view(template_name="about/category.html",
			id_page=3),
		name="kategorii"),
	# -------------------------------
	# для всех шаблонов, которые берутся из БД, если код страницы не найден,
	#то будет вставлен шаблон "page_in_development"
	url(r'^$', IndexPage.as_view(), name='main_page'),
	url(r'^(?P<path>.*)$', CommonTemplatePage.as_view(), name='common_template'),
]

"""
	обычные страницы, здесь их можно добавлять
	url_page и id_page берутся из таблицы основное меню из полей "ссылка" и id
	template_name - путь к html файлу, пример:
	url(r"^<url_page>/",
	CommonTemplatePage.run_view(template_name="about/kategorii.html",
								id_page=<id_page>), name="<name>")
"""